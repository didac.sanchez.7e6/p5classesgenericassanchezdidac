﻿using System;
using System.Collections.Generic;
namespace P5ClassesGenericasSanchezDidac
{
    /*--- Interficies ---*/
    public interface IOrdenable
    {
        int Comparar(IOrdenable x);
    };

    /*--- Classes ---*/
    class Program
    {
        /*--- Interficies ---*/
        public interface IOrdenable
        {
            int Comparar(IOrdenable x);
        };

        /*--- Classes ---*/

        abstract class FiguraGometrica : IOrdenable
        {

            /*--- Propiedades ---*/

            protected int Codi { get; set; }
            protected string Nom { get; set; }
            protected ConsoleColor Color { get; set; }

            /*--- Constructores ---*/

            public FiguraGometrica()
            {
                Codi = 0;
                Nom = "";
                Color = ConsoleColor.Black;
            }

            public FiguraGometrica(int codi, string nom, ConsoleColor color)
            {
                this.Codi = codi;
                this.Nom = nom;
                this.Color = color;
            }
            public FiguraGometrica(FiguraGometrica figura)
            {
                Codi = figura.Codi;
                Nom = figura.Nom;
                Color = figura.Color;
            }

            /*--- Get & Set ---*/

            public void SetCodi(int codi)
            {
                this.Codi = codi;
            }

            public void SetNom(string nom)
            {
                this.Nom = nom;
            }

            public void SetColor(ConsoleColor color)
            {
                this.Color = color;
            }

            public int GetCodi()
            {
                return this.Codi;
            }

            public string GetNom()
            {
                return this.Nom;
            }

            public ConsoleColor GetColor()
            {
                return this.Color;
            }

            /*--- Metodos Hererados ---*/
            public int Comparar(IOrdenable x)
            {
                FiguraGometrica fig = (FiguraGometrica)x;
                if (this.Area() == fig.Area()) return 0;
                if (this.Area() >= fig.Area()) return -1;
                return 1;
            }

            /*--- Metodos ---*/

            public override bool Equals(object obj)
            {
                FiguraGometrica figura = (FiguraGometrica)obj;
                return this.Codi == figura.Codi;
            }

            public override int GetHashCode()
            {
                return Convert.ToInt32(Math.Truncate(this.Codi * 2 - this.Codi / Math.PI));
            }

            public override string ToString()
            {
                return $"Codi : {this.Codi}, Nom : {this.Nom}, Color : {this.Color}";
            }

            abstract public double Perimetre();

            abstract public double Area();

        }

        class Retangle : FiguraGometrica
        {

            /*--- Atrivuts ---*/

            private double DistanciaBase { get; set; }
            private double Altura { get; set; }

            /*--- Constructores ---*/

            public Retangle() : base()
            {
                this.DistanciaBase = 0;
                this.Altura = 0;
            }
            public Retangle(double distanciaBase, double altura, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                this.DistanciaBase = distanciaBase;
                this.Altura = altura;
            }
            public Retangle(Retangle figura, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                DistanciaBase = figura.DistanciaBase;
                Altura = figura.Altura;
            }

            /*--- Get & Set ---*/

            public double GetDistanciaBase()
            {
                return DistanciaBase;
            }
            public void SetDistanciaBase(double DistanciaBase)
            {
                this.DistanciaBase = DistanciaBase;
            }
            public double GetAltura()
            {
                return Altura;
            }
            public void SetAltura(double Altura)
            {
                this.Altura = Altura;
            }

            /*--- Metodos ---*/

            public override string ToString()
            {
                return $"Base : {DistanciaBase}, Altur : {Altura} " + base.ToString();
            }
            public override double Area()
            {
                return this.DistanciaBase * this.Altura;
            }
            public override double Perimetre()
            {
                return this.DistanciaBase * 2 + this.Altura * 2;
            }

        }

        class Triangle : FiguraGometrica
        {

            /*--- Atrivuts ---*/

            private double DistanciaBase { get; set; }
            private double Altura { get; set; }

            /*--- Constructores ---*/

            public Triangle() : base()
            {
                this.DistanciaBase = 0;
                this.Altura = 0;
            }
            public Triangle(double distanciaBase, double altura, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                this.DistanciaBase = distanciaBase;
                this.Altura = altura;
            }
            public Triangle(Triangle figura, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                DistanciaBase = figura.DistanciaBase;
                Altura = figura.Altura;
            }

            /*--- Get & Set ---*/

            public double GetDistanciaBase()
            {
                return DistanciaBase;
            }
            public void SetDistanciaBase(double DistanciaBase)
            {
                this.DistanciaBase = DistanciaBase;
            }
            public double GetAltura()
            {
                return Altura;
            }
            public void SetAltura(double Altura)
            {
                this.Altura = Altura;
            }

            /*--- Metodos ---*/

            public override string ToString()
            {
                return $"Base : {DistanciaBase}, Altur : {Altura} " + base.ToString();
            }
            public override double Area()
            {
                return this.DistanciaBase * this.Altura;
            }
            public override double Perimetre()
            {
                return this.DistanciaBase * 2 + this.Altura * 2;
            }
        }

        class Cercle : FiguraGometrica
        {

            /*--- Parametres ---*/

            private double Radi;

            /*--- Constructores ---*/

            public Cercle() : base()
            {
                this.Radi = 0;

            }
            public Cercle(double radi, int codi, string nom, ConsoleColor color) : base(codi, nom, color)
            {
                this.Radi = radi;

            }
            public Cercle(Cercle figura) : base(figura)
            {
                this.Radi = figura.Radi;
            }

            /*--- Metodos ---*/

            public double GetRadi()
            {
                return Radi;
            }
            public void SetRadi(double radi)
            {
                this.Radi = radi;
            }
            public override string ToString()
            {
                return $"Radi : {Radi} " + base.ToString();
            }
            public override double Perimetre()
            {
                return (Radi * 2 * Math.PI);
            }
            public override double Area()
            {
                return (Math.Pow(Radi, 2) * Math.PI);
            }

        }

        public class TaulaOrdenable<T>
        {
            /*--- Propiedades ---*/
            public T[] Tabla { get; set; }
            public int Length;

            /*--- Constructor ---*/
            public TaulaOrdenable(T[] list)
            {
                Tabla = list;
                Length = list.Length - 1;
            }
            public string Capacitat()
            {
                return $"La capasitat maxima de la taula es {Length}";
            }
            public int NrElements()
            {
                int count = 0;
                foreach (T element in Tabla)
                {
                    if (element != null) count++;
                }
                return count;
            }
            public int Afegir(T element)
            {
                if (element == null) return -1;
                if (this.NrElements() == Length) return -2;
                Tabla[Length - (Length - this.NrElements())] = element;
                return 0;
            }
            public T ExemplarAt(int pos)
            {
                if (Length >= pos-1) return Tabla[pos-1];
                return default;
            }
            public void ExtreureAt(int pos)
            {
                Console.WriteLine(this.ExemplarAt(pos));
                Tabla[pos-1] = default;
            }
            public void Buidar()
            {
                for (int i = 0; i < Length; i++)
                {
                    Tabla[i] = default;
                }
            }
            public string Visualitzar()
            {
                string tabla = "";
                foreach (T element in Tabla)
                {
                    if (element != null) tabla += element + $"\n";
                }
                return $"{this.Capacitat()} te {this.NrElements()} ocupats i es de la classe {Tabla.GetType()} \n{tabla}";
            }

        }
        public class TaulaOrdenableFiguraGeometrica : TaulaOrdenable<IOrdenable>
        {
            public TaulaOrdenableFiguraGeometrica(int length) : base(new FiguraGometrica[Check(length)]) { }

        }
        public static int Check(int num)
        {
            if (num < 0) return 10;
            return num;
        }

        internal class Taules
        {
            public static List<IOrdenable> Ordenar(List<IOrdenable> obj)
            {
                obj.Sort((x, y) => x.Comparar(y));
                return obj;
            }
            static void Main()
            {
                TaulaOrdenableFiguraGeometrica taulaGeometrica = new TaulaOrdenableFiguraGeometrica(50);
                taulaGeometrica.Afegir(new Cercle(5, 1, "circulo", ConsoleColor.Red));
                taulaGeometrica.Afegir(new Cercle(25, 2, "360º", ConsoleColor.Blue));
                taulaGeometrica.Afegir(new Retangle(3, 5, 3, "90º", ConsoleColor.Black));
                taulaGeometrica.Afegir(new Triangle(3, 5, 4, "Rectangulo", ConsoleColor.Green));
                taulaGeometrica.Afegir(new Triangle(100, 90, 5, "Esqueleto", ConsoleColor.White));
                Console.WriteLine(taulaGeometrica.Visualitzar());
                Console.WriteLine(taulaGeometrica.ExemplarAt(3));
                taulaGeometrica.ExtreureAt(3);
                Console.WriteLine(taulaGeometrica.Visualitzar());
                taulaGeometrica.Buidar();
                Console.WriteLine(taulaGeometrica.Visualitzar());
            }
        }
    }
}
